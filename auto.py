#!/usr/bin/python3
# -*- coding: utf-8 -*-
# pylint: disable=invalid-name, missing-docstring

"""
TP AP1
Licence SESI 1ère année
Univ. Lille 1

Scipt permettant d'automatiser le rendu des TP
"""

__author__ = "PREUD'HOMME Geoffrey"

import os
import zipfile

base = './'
rendeurs = [('BEAUSSART Jean-loup', 'Beaussart'),
            ('PREUD\\\'HOMME Geoffrey', 'PreudHomme')]


def chemin(semestre, tp=None):
    return os.path.normpath(base + '/S' + str(semestre) + ('/TP' + str(tp) if tp else ''))


def semestreProchain():
    i = 1
    while os.path.exists(chemin(i)):
        i += 1
    return i


def semestreEnCours():
    return semestreProchain() - 1


def tpProchain():
    i = 1
    semestre = semestreEnCours()
    while os.path.exists(chemin(semestre, i)):
        i += 1
    return i


def tpEnCours():
    return tpProchain() - 1


def fichiersTp(semestre, tp):
    # TODO .gitignore
    # TODO .tpfiles
    fichiers = fichiersPythons(semestre, tp)
    if semestre == 2:
        if tp == 3:
            fichiers.remove('bataille_navale_graphique.py')
            fichiers.append('jeu3.txt')
        if tp == 4:
            fichiers.append('Makefile')
    return fichiers


def fichiersPythons(semestre, tp):
    chem = chemin(semestre, tp)
    return [i for i in os.listdir(chem) if i.endswith('.py') and os.path.isfile(os.path.join(chem, i))]


def rendeur(semestre, tp):
    for i in fichiersPythons(semestre, tp):
        texte = open(os.path.join(chemin(semestre, tp), i), 'r').read()
        rend = ''
        rendeurMax = 500
        for j in range(len(rendeurs)):
            pos = texte.find(rendeurs[j][0])
            if pos < rendeurMax and pos >= 0:
                rendeurMax = pos
                rend = j
        if rend != '':
            return rend
    return None


def personnesSurTP(semestre, tp):
    rend = rendeur(semestre, tp)
    personnes = [i[1] for i in rendeurs]
    personnes.remove(rendeurs[rend][1])
    personnes = [rendeurs[rend][1]] + personnes
    return personnes

def creerZip(semestre, tp):
    personnes = personnesSurTP(semestre, tp)
    nomDossier = '_'.join(personnes)
    nomZip = 'tp%d_%s.zip' % (tp, ('_'.join(personnes).lower()))
    chem = chemin(semestre, tp)
    fichierZip = zipfile.ZipFile(os.path.join(chem, nomZip), 'w', zipfile.ZIP_DEFLATED)
    for f in fichiersTp(semestre, tp):
        fichierZip.write(os.path.join(chem, f), os.path.join(nomDossier, f))
    fichierZip.close()

# os.link(chemin(semestreEnCours(), tpEnCours()), 't')
# os.link(chemin(semestreEnCours()), 's')
creerZip(semestreEnCours(), tpEnCours())
