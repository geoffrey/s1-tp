#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
TP AP1
Licence SESI 1ère année
Univ. Lille 1

IA bataille navale

"""

__author__ = 'BEAUSSART Jean-loup & PREUD\'HOMME Geoffrey'
__date_creation__ = 'Mon, 16 Feb 2015 19:30:54 +0100'

import bataille_navale as BN
from random import randint


def jouerIA(nom, descr, niveau):
    """
    str, str → ()
    procédure de jeu complet de bataille navale,
    le nom du joueur est donné par le paramètre nom,
    et le jeu est décrit dans le fichier descr.

    CU : le fichier jeu doit exister et être conforme
    à un fichier de description.
    """
    jeu = BN.cree_jeu(descr)
    BN.decrire_le_jeu(jeu)
    nbre_tirs = 0
    res = False
    while not BN.tous_coules(jeu):
        BN.afficher_jeu(jeu)
        tir = choisir_tir(jeu, res, niveau)
        nbre_tirs += 1
        nav, res = BN.analyse_un_tir(jeu, tir)
        if res == BN.RATE:
            print("raté.")
        elif res == BN.TOUCHE:
            print(nav + " touché.")
        else:
            print(nav + " coulé.")
    BN.sauver_result(nom+' (IA)', descr, nbre_tirs)
    # On rajoute IA dans les scores pour repérér les tricheurs
    print("Terminé en %d tirs" % nbre_tirs)

def choisir_tir(jeu, res, niveau):
    niveaux = [choisir_tir_1, choisir_tir_2, choisir_tir_3]
    fonction = niveaux[niveau - 1]
    tir = fonction(jeu, res)
    print('Tir chosi :', tir)
    return tir

def choisir_tir_1(jeu, res):
    """
    dict, int → (int, int)
    IA de bas niveau, tire aléatoirement dans la grille
    """
    x = randint(0, jeu['plateau']['larg'])
    y = randint(0, jeu['plateau']['haut'])
    return (x, y)

def choisir_tir_2(jeu, res):
    """
    dict, int → (int, int)
    IA moyenne, tire aléatoirement mais jamais deux fois au même endroit
    """
    x,y= randint(1, jeu['plateau']['larg']), randint(1, jeu['plateau']['haut'])

    while (x,y) in jeu['coups_joues']:
        x = randint(1, jeu['plateau']['larg'])
        y = randint(1, jeu['plateau']['haut'])

    return (x, y)

def en_bonds(esp, c):
    """
    dict, tuple → bool
    Indique si la position c est dans l'espace maritime esp
    """
    return not (c[0] > esp['larg'] or c[0] <= 0 or c[1] <= 0 or c[1] > esp['haut'])

def choisir_tir_3(jeu, res):
    """
    dict, int → (int, int)
    IA bonne, prend en priorité les cases adjacentes à celles touchées
    """
    # Choix du mode
    if 'ia' not in jeu: # On crée un dict qui stockera quelques variables utiles 
        jeu['ia'] = dict()
        jeu['ia']['mode'] = 0 # 0 : aléatoire, 1 : cases adjacentes à la dernière touche
        #jeu['ia']['cl'] = -1
    if type(res) == int:
        if res == BN.COULE:
            jeu['ia']['mode'] = 0 # Retour en mode aléatoire
        elif res == BN.TOUCHE:
            jeu['ia']['d_touche'] = jeu['ia']['d_coup'] # Stockage de a dernière touche
            jeu['ia']['mode'] = 1

    # Acteur
    if jeu['ia']['mode'] == 1: # Si en mode adjacent
        proxi = [(0, 1), (0, -1), (1, 0), (-1, 0)] # Cases de proximité
        for i in proxi:
            x = jeu['ia']['d_touche'][0] + i[0]
            y = jeu['ia']['d_touche'][1] + i[1]
            coup = (x, y)
            if coup not in jeu['coups_joues'] and en_bonds(jeu['plateau'], coup):
                break # Si coup non joué et dans le plateau, on valide
            if i == proxi[-1]: # Si aucune case adjacente ne fonctionne,
                               # retour en mode aléatoire
                               # (arrive quand 2 navires sont cote à cote)
                jeu['ia']['mode'] = 0
    if jeu['ia']['mode'] == 0: # Si en mode aléatoire
        while True:
            x = randint(1, jeu['plateau']['larg'])
            y = randint(1, jeu['plateau']['haut'])
            coup = (x, y)
            if coup not in jeu['coups_joues']:
                break
    jeu['ia']['d_coup'] = coup # Stockage du dernier coup joué
    return coup


if __name__ == '__main__':
    import sys
    if len(sys.argv) != 4:
        jouerIA('Pirate borgne', '1', 3)
    else:
        jouerIA(sys.argv[1], sys.argv[2], int(sys.argv[3]))
