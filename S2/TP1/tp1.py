# BEAUSSART Jean-loup
# PREUD'HOMME Geoffrey
# Donné le 20/01/2015
# TP1 Gestion d'une promotion d'étudians - Structures itérables

# http://www.fil.univ-lille1.fr/~L1S2API/CoursTP/tp1_structures_iterables.html

from etudiants import *


def question(numero):
    print('\n***', 'Question', numero, '***')

question(1)
# Vérifiez que la variable l_etudiants est bien du type décrit ci-dessus,
# c’est-à-dire une liste de quintuplets, chaque quintuplet étant composé
# de quatre chaînes de caractères et un nombre.

test1 = type(l_etudiants) == list
for i in l_etudiants:
    if not (type(i) == tuple and len(i) == 5 and type(i[4]) == int):
        test1 = False
    for j in range(4):
        if not type(i[j]) == str:
            test1 = False


print('Le test a retourné', test1)


question(2)
# Combien de fiches d’étudiants contient la liste l_etudiants ?

print('Il y a', len(l_etudiants), 'étudiants dans la liste')


question(3)
# Quel est le contenu de la fiche se trouvant à l’indice égal à votre
# numéro d’étudiants modulo le nombre de fiches ?

monId = 11501230  # Jean-Loup
# monId = 11500683 # Geoffrey

fiche = l_etudiants[monId % len(l_etudiants)]
print('Cette fiche est celle de', fiche[2], fiche[1],
      'qui est en', fiche[3], fiche[4], 'et son id est le', fiche[0])

question(4)
# Vérifiez que dans la liste tous les étudiants sont bien dans l’une des
# quatre formations mentionnées ci-dessus.

test4 = True
for i in l_etudiants:
    if not i[3] in ('LICAM', 'MASS', 'PEIP', 'SESI'):
        test4 = False

print('Le test a retourné', test4)


question(5)
# Réalisez une fonction nommée nbre_prenoms qui renvoie le nombre
# d’étudiants dont le prénom est passé en paramètre. Combien d’étudiants
# se prénomment-ils Alexandre ? et Camille ?


def nbre_prenoms(prenom):
    """
    str → int
    Renvoie le nombre d’étudiants dont le prénom est passé en paramètre
    CU : prenom est un str
    """
    assert(type(prenom) == str), 'prenom n\'est pas du type str'

    return [i[2] for i in l_etudiants].count(prenom.upper())

print('Il y a', nbre_prenoms('Alexandre'),
      'étudiants qui s\'appellent Alexandre')
print('Il y a', nbre_prenoms('Camille'),
      'étudiant(e)s qui s\'appellent Camille')

question(6)
# Combien y a-t-il de prénoms différents parmi tous les étudiants ?

ensemblePrenoms = set([i[2] for i in l_etudiants])
print('Il y a', len(ensemblePrenoms),
      'prénoms différents parmi tous les étudiants')

question(7)
# Quel est le prénom le plus fréquent ?

# La question demande "quel est le prénom le plus fréquent", nous nous
# sommes permis ici de répondre à la question "quels sont les prénoms les
# plus fréquents"

nbresPrenoms = dict((i.upper(), nbre_prenoms(i)) for i in ensemblePrenoms)

prenomsPlusFrequent = set()
nbrePrenomsPlusFrequents = 0
for i in nbresPrenoms:
    if nbresPrenoms[i] > nbrePrenomsPlusFrequents:
        prenomsPlusFrequent = {i}
        nbrePrenomsPlusFrequents = nbresPrenoms[i]
    elif nbresPrenoms[i] == nbrePrenomsPlusFrequents:
        prenomsPlusFrequent.add(i)

# Décommentez la ligne suivante pour tester le cas où plusieurs prénoms arriveraient au même rang
# prenomsPlusFrequent.add('MAURICE')

terminaison = 's' if len(prenomsPlusFrequent) > 1 else ''
print('Le' + terminaison + ' prénom' + terminaison + ' le' + terminaison + ' plus fréquent' +
      terminaison, 'sont' if len(prenomsPlusFrequent) > 1 else 'est',
      ', '.join(prenomsPlusFrequent))

question(8)
# Vérifiez que les identifiants (id) des étudiants sont tous distincts.

idUniques = set(i[0] for i in l_etudiants)

test8 = len(idUniques) == len(l_etudiants)

print('Le test a retourné', test8)

question(9)
# En un seul parcours de la liste des étudiants, déterminez le nombre
# d’étudiants dans chacune des formations.

parcours = dict()

for i in l_etudiants:
    if not i[3] in parcours:
        parcours[i[3]] = 0
    parcours[i[3]] += 1

print('Il y a', ', '.join(
    list(str(parcours[i]) + ' étudiants en ' + i for i in parcours)) + '.')

question(10)
# Réalisez une fonction nommée liste_formation qui construit la liste des
# étudiants de la formation donnée en paramètre. Cette liste contiendra
# des quadruplets (id, nom, prenom, gpe).


def liste_formation(formation):
    """
    str → list[tuple(str, str, str, int)]
    Renvoie la liste des quadruplets (id, nom, prenom, gpe) correspondants à tous les étudiants
    appartenant à la formation donnée
    CU : formation est une formation valide
    """
    assert(formation in parcours), 'La formation donnée n\'est pas valide'

    return list(i[0:2] + (i[4],) for i in l_etudiants if i[3] == formation)
