#!/usr/bin/python3
# -*- coding: utf-8 -*-
# pylint: disable=invalid-name

"""
TP AP1
Licence SESI 1ère année
Univ. Lille 1

analyse_en_moyenne.py

TP4 - Evaluation empirique des tris
Analyse du coût moyen du tri par insertion

http://www.fil.univ-lille1.fr/~L1S2API/CoursTP/tp4_tri.html

"""

# Analyse des arguments
import argparse

parser = argparse.ArgumentParser(
    description='Analyse le coût moyen du tri par insertion.')
parser.add_argument(
    '--brut', action='store_true', help="afficher les données brutes")
parser.add_argument(
    '--poly', action='store_true', help="calculer la regression polynôminale")
parser.add_argument(
    '--graph', action='store_true', help="voir les données sous forme de graphique")
parser.add_argument('-m', type=int, default=100, help="Changer la valeur de m")

args = parser.parse_args()


m = args.m
from analyse_tris import nbre_moyen_tri_insertion

# Affichage des données

xData = list(range(1, 101))
yData = []
for i in range(len(xData)):
    yData.append(nbre_moyen_tri_insertion(m, i))
    if args.brut:
        print("{:<3}  {:>14}".format(xData[i], "{:0.2f}".format(yData[i])))

# Régression polynominale
from numpy import polyfit
if args.poly:
    polynome = polyfit(xData, yData, 2)
    if args.brut:
        print("f(x) = {}".format(' '.join(["{:+f} × x^{}" \
              .format(polynome[d], len(polynome) - 1 - d) for d in range(len(polynome))])))


# Affichage
from matplotlib import pyplot
if args.graph:
    pyplot.plot(xData, yData, 'x', label="Données brutes")
    if args.poly:
        if args.poly:
            def f(x):
                """
                float → float
                Retourne un point de la regression polynôminale de l'analyse du tri.
                CU : polynome est défini
                """
                y = 0
                for d in range(len(polynome)):
                    y += polynome[d] * x ** (len(polynome) - 1 - d)
                return y
        pyplot.plot(xData, [f(x)
                            for x in xData], '-', label="Régression polynôminale")
    pyplot.legend(loc='upper left')
    pyplot.xlabel("n")
    pyplot.ylabel("comparaisons")
    pyplot.title("Nombre de comparaisons faite par le tri par insertion en fonction de la longueur \
de la liste")
    pyplot.show()
