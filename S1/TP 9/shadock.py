# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°9 donné le 21/11/2014 - L’alphabet shadok
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_numeration_shadok.html

import doctest


# À l’aide de la fonction chr, définissez quatre constantes GA, BU, ZO et
# MEU ayant pour valeur les quatre chiffres shadoks.

GA = chr(0x004F)
BU = chr(0x2212)
ZO = chr(0x2A3C)
MEU = chr(0x25FF)


# Définissez une variable ALPHABET_SHADOK dont la valeur est la liste des
# quatre chiffres shadoks dans l’ordre O, −, ⨼ et ◿.

ALPHABET_SHADOK = [GA, BU, ZO, MEU]


# Réalisez une fonction entier_en_shadok qui renvoie l’écriture shadok de
# l’entier passé en paramètre. Contentez-vous d’une fonction qui convient
# pour les entiers positifs ou nuls.


def entier_en_shadok(d):
    """
    Renvoie l’écriture shadok de l’entier positif passé en paramètre.
    CU : d positif ou nul
    int → str

    >>> entier_en_shadok(0)
    'O'
    >>> entier_en_shadok(1)
    '−'
    >>> entier_en_shadok(2)
    '⨼'
    >>> entier_en_shadok(3)
    '◿'
    >>> entier_en_shadok(13)
    '◿−'
    """
    assert(type(d) is int and d >= 0), "d doit être un entier positif ou nul"

    if d == 0:
        return GA
    else:
        D = d
        r = ''
        while D >= 1:
            r = ALPHABET_SHADOK[D % 4] + r
            D = D // 4
        return r


# Quelle est l’écriture shadok de 2014 ?

# −◿◿−◿⨼


# Réalisez la fonction nommée shadok_en_entier réciproque de la précédente
# qui renvoie le nombre correspondant à une écriture shadok.


def shadok_en_entier_unique(s):
    """
    Renvoie le nombre correspondant à un symbole shadok
    str → int

    >>> shadok_en_entier(GA)
    0
    >>> shadok_en_entier(BU)
    1
    >>> shadok_en_entier(ZO)
    2
    >>> shadok_en_entier(MEU)
    3
    """
    if s == GA:
        return 0
    elif s == BU:
        return 1
    elif s == ZO:
        return 2
    elif s == MEU:
        return 3
    else:
        assert(False), "Pas un symbole shadok"


def shadok_en_entier(s):
    """
    Renvoie le nombre correspondant à une écriture shadok
    str → int

    >>> shadok_en_entier(GA)
    0
    >>> shadok_en_entier(BU)
    1
    >>> shadok_en_entier(ZO)
    2
    >>> shadok_en_entier(MEU)
    3
    >>> shadok_en_entier(MEU+BU)
    13
    """
    S = s
    d = 0
    if len(S) > 0:
        for i in range(-1, -len(S) - 1, -1):
            d += shadok_en_entier_unique(S[i]) * 4 ** (-i - 1)
    return d


# Répondez à la question posée à la fin de la vidéo : quel est l’entier
# correspondant au nombre écrit − ⨼ O ◿ ?

# >>> shadok_en_entier('−⨼O◿')
# 99

# J'ai malheureusement peur qu'il ne soit trop tard pour le professeur Shadoko


# Messages Shadok

# Expliquez pourquoi tout nombre entier représentable sur un octet peut
# être écrit en shadok avec quatre chiffres shadoks.

# Il y a 2^8=256 possibilités de nombres entier pouvant se représenter sur un
# octet. Il y a aussi 4^4=256 possibilités d'ensembles de 4 chiffres en base 4
# (ou de chiffres shadoks). On peut donc trouver un équivalent entre les deux


# Réalisez une fonction nommée octet_en_shadok qui renvoie l’écriture
# shadok avec exactement quatre chiffres d’un entier représentable sur un
# octet.

def octet_en_shadok(o):
    """
    Renvoie l’écriture shadok avec exactement quatre chiffres d’un entier
    représentable sur un octet
    int → str

    >>> octet_en_shadok (0)
    'OOOO'
    >>> octet_en_shadok (255)
    '◿◿◿◿'
    >>> octet_en_shadok (65)
    '−OO−'
    """
    assert(type(o) is int and o < 256), 'o doit être représentable sur un seul \
octet'
    s = entier_en_shadok(o)
    while len(s) < 4:
        s = GA + s
    return s


# Quel est la chaîne de caractères shadoks qui code le caractère e ?

# >>> octet_en_shadok(ord('e'))
# '−⨼−−'


# Réalisez une fonction code_car_en_shadok qui code un caractère ASCII en
# shadok.


def code_car_en_shadok(c):
    """
    Code un caractère ASCII en shadok
    str → str
    CU : c un unique caractère

    >>> code_car_en_shadok('A')
    '−OO−'
    >>> code_car_en_shadok('e')
    '−⨼−−'
    """
    assert(type(c) is str and len(c) == 1), "c doit être un unique caractère"

    return octet_en_shadok(ord(c))

# Réalisez une fonction code_en_shadok qui code en shadok la chaîne de
# caractères ASCII passée en paramètre.


def code_en_shadok(s):
    """
    Code une chaîne de caractères ASCII en shadok
    str → str
    CU : c str

    >>> code_en_shadok ('Timoleon')
    '−−−O−⨼⨼−−⨼◿−−⨼◿◿−⨼◿O−⨼−−−⨼◿◿−⨼◿⨼'
    """
    assert(type(s) is str), "s doit être un str"

    r = ''
    for i in s:
        r += code_car_en_shadok(i)
    return r


# Quel est le caractère ASCII codé par la chaîne ‘O⨼⨼⨼’ ?

# >>> shadok_en_entier('O⨼⨼⨼')
# 42

# Et le caractère ASCII associé à la Grande Question sur la vie, l'univers et
# le reste est ...

# >>> chr(42)
# '*'

# * étant le caractère joker sous les systèmes de type Unix qui correspond à
# tous les élément d'une liste. Ce qui possède un sens en soi.

# Plus sérieusement, la réponse est *


# Est-il possible que la chaîne ‘⨼◿◿−’ corresponde à un caractère ASCII ?

# >>> shadok_en_entier('⨼◿◿−')
# 189

# Or 189 est supérieur à 127, qui est le dernier caractère dans la table ASCII.

# >>> bin(189)
# '0b10111101'

# Cependant, on obtient un nombre pair de chiffres 0 et de chiffres 1, pour
# l'ASCII standard, possédant un bit de contrôle, ce caractère est valide

# Cependant, Python 3 utilisant par défaut l'UTF-8, notammennt la fonction chr()
# on obtiendra le caractère UTF-8 correspondant à l'octet 0b10111101.

# >>> chr(shadok_en_entier('⨼◿◿−'))
# '½'


# Réalisez une fonction decode_car_du_shadok qui décode une chaîne de
# quatre caractères shadoks en un caractère ASCII. (Réfléchissez bien sur
# les contraintes d’utilisation)

def decode_car_du_shadok(s):
    """
    Décode une chaîne de quatre caractères shadoks en un caractère ASCII
    str → str
    CU : s str composé de 4 caractères parmi GA, BU, ZO ou MEU correspondant à
    un caractère ASCII existant (le bit de contrôle étant égal à 0)

    >>> decode_car_du_shadok('−OO−')
    'A'
    >>> decode_car_du_shadok('O⨼⨼⨼')
    '*'
    """
    assert(type(s) is str and len(s) == 4), "s doit être un str composé de 4 \
caractères"
    d = shadok_en_entier(s)
    assert(d < 128), "s doit correspondre à un caractères ASCII existant"
    return chr(d)


# Réalisez une fonction decode_du_shadok qui décode une chaîne shadok.

def decode_du_shadok(s):
    """
    Décode une suite d'ensembles de quatre caractères shadoks en caractère ASCII
    str → str
    CU : s str composé d'ensembles de 4 caractères parmi GA, BU, ZO ou MEU
    correspondant à un caractère ASCII existant (le bit de contrôle étant égal à
    0)

    >>> decode_du_shadok('−−−O−⨼⨼−−⨼◿−−⨼◿◿−⨼◿O−⨼−−−⨼◿◿−⨼◿⨼')
    'Timoleon'
    """
    assert(type(s) is str and len(s)%4 == 0), "s doit être un str composé \
d'ensembles de 4 caractères"

    r = ''
    for i in range(0, len(s), 4):
        r += decode_car_du_shadok(s[i:i+4])
    return r

# Décodez le message ‘−O−◿−OO−−OO⨼−−−−−−⨼⨼−O◿◿−O◿−−O−−−−−−’.

def tester():
    doctest.testmod(verbose=True)
