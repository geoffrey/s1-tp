# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°4 donné le 3/10/2014 - Choisis un nombre
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_iteration_conditionnelle.html


from random import randint

# [Q1] Que fait la fonction randint ?

# Utilisée sous la forme randint(a, b), a et b étant des entiers, elle retourne
# un entier pseudo-aléatoire entre a et b (différent à chaque éxecution)

# [Tests]
# >>> randint(0, 100)
# 39
# >>> randint(-500, 500)
# -161
# >>> randint(0, 1)
# 1

# [Q2] Créez une variable nommée mystere contenant un nombre entier
# tiré (pseudo-)aléatoirement entre 1 et 100.
mystere = randint(0, 100)

# [Q3] Que fait la fonction input ?

# Utilisée sous la forme input(a), elle affiche le texte a et autorise la
# saisie dans le terminal tout en bloquant l'execution du script. Après l'appui
# de la touche Entrée, la fonction se débloque et renvoie le texte de la saisie

# Quel est le type des valeurs renvoyées par cette fonction ?

# Le type renvoyé par input est une chaîne de caractrèes (<class 'str'>)

# [Q4] Écrivez une ou plusieurs instructions qui demandent à
# l’utilisateur d’entrer un nombre.

# print("[Q4]")
# reponse1 = input("En quelle année a été assassiné Louis XVI ? ")
# reponse2 = input("Quelles sont les 10 premières décimales de π ? ")
# reponse3 = input("J'ai dix billes que je partage équitablement entre trois \
#     de mes camarades, combien me reste-t-il de bille(s) ? ")

# [Q5-7] (le code est majoritairement issu de la Q5, ce qui a été
# ajouté après est identifié comme telen commentaire)
def jeu():
    """
    Lance un jeu de + ou -.

    CU : nombre minimum inférieur au nombre maximum,
    nombre d'essais supérieur à 0
    """
    print("Jeu du + ou -")
    continuer_jeu = True # Cette variable permet de savoir si il faut continuer
    # à demander un nombre ou non.

    # On demande au joueur les valeurs avec lesquels il veut jouer
    minimum = int(input("Entrez le nombre minimum : ")) # Q7
    maximum = int(input("Entrez le nombre maximum : ")) # Q7
    assert(minimum < maximum), "Le nombre minimum doit être inférieur au \
        nombre maximum" # Q7
    maxEssais = int(input("Entrez le nombre d'essais : ")) # Q7
    assert(maxEssais > 0), "Le nombre d'essais doit être supérieur à 0" # Q7
    # minimum = 1 # Pré-Q7
    # maximum = 100 # Pré-Q7
    # maxEssais = 1 # Pré-Q7
    essais = 0 # Comptabilise les essais réalisés
    # J'utilise un autre nom de variable pour être conforme à la règle PEP 3104
    mystere2 = randint(minimum, maximum) # On pioche le nombre mystère
    # print("Nombre à trouver :", mystere2) # Décommenter pour tricher
    while continuer_jeu:
        proposition = int(input("Entrez un nombre entre "+str(minimum)+" et "+\
            str(maximum)+" ("+str(maxEssais - essais)+" essais restants): "))
        if proposition >= minimum and proposition <= maximum: # Si le nombre est
        # en dehors de l'intervalle, on ne fait rien (pas même compter un essai)
        # et on revient à l'affichage du essage
            if proposition == mystere2:
                print("C'est GAGNÉ")
                continuer_jeu = False # On coupe la boucle
            else: # Ce décalage permet, en cas de victoire, de ne pas afficher
            # comme quoi le nombre d'essai est dépassé ni de compter un essai
            # supplémentaire
                essais += 1 # On comptabilise l'essai
                if proposition < mystere2: # Si le nombre est inférieur
                    print("C'est PLUS")
                elif proposition > mystere2: # Si le nombre est supérieur
                    print("C'est MOINS")
                if essais >= maxEssais: # Q6
                    print("Vous avez dépassé le nombre d'essais autorisés") # Q6
                    continuer_jeu = False # Q6
    score = int((7**2 * (maximum - minimum))/(maxEssais*99)-essais) # Q7
    # J'utilise cette méthode de calcul de score car :
    # - Elle donne les mêmes résultat que le jeu décrit dans le sujet pour un
    #     intervalle de [1,100] et un nombre d'essai de 7
    # - Elle est est proportionelle à l'intervalle entre les bornes du choix
    # - Elle est est inversement proportionelle au nombre d'essais donné
    # - Elle me parait relativement équitable
    # - Elle est tronquée pour la lisibilité

    # score = 7 - essais # Pré-Q7
    print("Vous avez marqué", score, "points.")
