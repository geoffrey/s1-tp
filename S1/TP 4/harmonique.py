# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°4 donné le 3/10/2014 - Série harmonique alternée
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_iteration_conditionnelle.html

import doctest

# [Q1] Écrivez une fonction somme_partielle
def somme_partielle(n):
    """
    Renvoie la valeur de la somme de i=1 à n de (-1)^i/i.

    CU : n entier

    Exemple :
    >>> somme_partielle(5)
    -0.7833333333333332
    >>> somme_partielle(3145)
    -0.6933061377964356
    """
    assert(type(n) is int), "n doit être un entier"

    somme = 0
    for i in range(1, n+1):
        # On simplifie (-1)**i pour éviter de faire
        # trop de calculs
        if i % 2 == 0: # Si i est pair
            i_puissance = 1 # (-1)**i = 1
        else: # Si i est impair
            i_puissance = -1 # (-1)**i = 1
        somme += i_puissance/i
    return somme

def tester():
    doctest.testmod(verbose=True)
