# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°4 donné le 3/10/2014 - Compte à rebours
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_iteration_conditionnelle.html


# [Q1] Programmez une procédure nommée compte_a_rebours
def compte_a_rebours(depart):
    """
    Affiche le décompte jusque 0 à partir de depart.

    CU : depart entier > 0
    """
    assert(type(depart) is int and depart > 0), \
        "depart doit être un entier supérieur à 0"

    for i in range(depart, -1, -1):
        print(i)

# [Test]
# >>> compte_a_rebours(10)
# 10
# 9
# 8
# 7
# 6
# 5
# 4
# 3
# 2
# 1
# 0
