# Du gros n'importe quoi pour la déconnade

from random import *
from time import sleep
from sys import stdout

def dire(propositions="", fin="\n"):
    if propositions == "":
        sleep(1)
    else:
        phrase = choice(propositions)
        for i in range(0, len(phrase)):
            print(phrase[i], end='')
            stdout.flush()
            if phrase[i] in ['.', ',', '!', '?', '\n']:
                sleep(0.5)
            else:
                sleep(0.02)
    print(end=fin)

def jeu():
    continuer_jeu = True

    minimum = 1
    maximum = 100
    maxEssais = 7
    essais = 0
    mystere = randint(minimum, maximum)
    dire()
    if random() < 0.2:
        dire(["Donc je prend un nombre, tu vois, genre "+str(mystere)+", et tu dois le trouver, OK ?", str(mystere-2)+" ? "+str(mystere-1)+" ? "+str(mystere)+", c'est bien ça, "+str(mystere)+"...", str(mystere)+". EUH ! C'est pas "+str(mystere)+", pas du tout !"])
    else:
        dire(["Ayé, trouvé !", "Attends je cherche un nombre sympa...\nC'est bon !", "...\nAh oui ! Un nombre.\n...\nVoilà !","Je viens de penser à un nombre, tu vas devoir le trouver."])
    while continuer_jeu:
        dire()
        dire(["Vas-y, je t'écoute.","Alors ?","Un nombre entre 1 et 100, oublie pas.","Euh, je crois que j'ai oublié le nombre...\n\nAh non, c'est bon ! Je l'ai retrouvé !", "Rzzz...\nRzzz...\nRzzz...\nHmm ? Ah oui, pardon.", "ATTENTION, DERRIÈRE TOI, UNE GUÈPE !\n\nNon je déconne, je voulais te faire perdre où tu en étais.","Nous t'écoutons.","Mais, qui es-tu ?\nAh oui, c'est vrai, tu viens jouer.","Comment tu t'appelle ?\nAh non, c'est pas ça la question.","Poooo, Polytech !\nOui pardon je t'écoute","CECI EST UN HOLD-UP, DONNE MOI TON NOMBRE !"], " ")
        proposition = int(input())
        if proposition >= minimum and proposition <= maximum:
            essais += 1
            if proposition == mystere:
                dire(["C'est GAGNÉ", "C'est ça. Je crois","Eh, mais comment t'as fait ?","C'est PLUS.\n\n\nEuh, attends, non, c'est ça ! T'as gagné là !","C'est pas ça ? Ah si, c'est ça.","Non, tu n'as pas gagné, c'est pas ça.","Putain !\nT'as triché non ?","Alors là, non, t'as triché c'est pas possible","Tricheur."])
                continuer_jeu = False
            elif proposition < mystere:
                dire(["C'est PLUS","C'est un peu plus","C'est plus, c'est moins, en tout cas c'est pas ça.","T'es juste en dessous !","Trop bas","Ah ah, je te dirai rien !","Sérieusement, tu crois ? Bah non, c'est PLUS.","Non, c'est beaucoup plus","Même mon chat il joue mieux !"])
            elif proposition > mystere:
                dire(["C'est MOINS","C'est un peu moins","C'est moins, c'est plus, en tout cas c'est pas ça.","T'es juste au dessus !","Trop haut","Mouhahah, tu crois que je vais parler ?","Vraiment, tu pense ? Eh non, c'est MOINS.","Non, c'est beaucoup moins","Même mon chien il joue mieux !"])
        if essais >= maxEssais and continuer_jeu:
            dire(["DING ! T'as épuisé tout ton crédit !","Fini ! Concentre-toi un peu","Perdu ! Et ça se dit pro...","Eh, réflechi un peu de temps en temps !"])
            continuer_jeu = False
    score = int((7**2 * (maximum - minimum))/(maxEssais*99)-essais+1)
    if score >= 4:
        dire([str(score)+" points, c'est pas mal.","T'as marqué "+str(score)+" points. Bien !","T'ain, t'as tout déchiré ! "+str(score)+" points !","Tu vas me ruiner avec tes "+str(score)+" points !\nAh, non, c'est pas un jeu d'argent","Tu m'énerve...\nTellement que je te dirais pas que t'as "+str(score)+" points.","Ah le salaud !"])
    else:
        dire(["Noob. "+str(score)+" points c'est digne d'un noob.","OH MON DIEU ! C'EST EXTRAORDINAIRE ! "+str(score)+" POINTS !\nAh non c'est naze en fait.","Et ça se dit pro avec ses "+str(score)+" points !",str(score)+"/7, insuffisant.",str(score)+" points. T'as ton bac au moins ?","HAHA LE NUL ! QUE "+str(score)+" POINTS !"+"Nul, nul, nul."])

dire(["Yo ! Bienvenue au jeu du plus ou moins !", "Salut ! Ça roule ? Tu veux jouer avec moi au plus ou moins ?", "...\nAh, salut ! Tu veux jouer au plus ou moins ?"])
while True:
    jeu()
    dire("", "\n\n\n")
    dire(["On refait une partie ?", "Toujours là ? Bon, eh bah c'est reparti !", "Tu t'arrête quand ?", "Bon, on en refait une, je peux pas rester sur une défaite.", "Allez on rejoue !"])
