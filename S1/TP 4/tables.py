# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°4 donné le 3/10/2014 - Tables de multiplication
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_iteration_conditionnelle.html

# [Q1] Écrivez une procédure qui affiche la table de multiplication par
# un nombre k sous la forme donnée ci-après.
def imprimer_table(k):
    """
    Affiche la table de multiplication par un nombre k.

    CU : k entier
    """
    assert(type(k) is int), "k doit être un entier"

    for i in range(1, 11):
        print(k, "×", i, "=", k*i)

# [Test]
# >>> imprimer_table(7)
# 7 x 1 = 7
# 7 x 2 = 14
# 7 x 3 = 21
# 7 x 4 = 28
# 7 x 5 = 35
# 7 x 6 = 42
# 7 x 7 = 49
# 7 x 8 = 56
# 7 x 9 = 63
# 7 x 10 = 70

# [Q2] Donnez une séquence d’instructions permettant d’avoir l’affichage
# de toutes les tables de multiplication de 1 jusqu’à 10.
# (En commentaire pour éviter de polluer la console)

for table in range(1, 11):
    imprimer_table(table)
