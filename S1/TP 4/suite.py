# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°4 donné le 3/10/2014 - Suite arithmético-géométrique
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_iteration_conditionnelle.html

import doctest

# [Q1] Créez une variable u_0, u_1 et u_2
u_0 = 0
u_1 = 3*u_0+1
u_2 = 3*u_1+1

# [Q2] Définissez une fonction u_terme qui renvoie la valeur de u_n.
def u_terme(n):
    """
    Renvoie la valeur de u_n.

    CU : n ≥ 0

    Exemple:
    >>> u_terme(0)
    0
    >>> u_terme(59)
    7065193045869367252382405533
    """
    assert(type(n) is int and n >= 0), "n doit être un entier positif"

    u = 0
    for i in range(1, n+1):
        u = 3*u+1
    return u

# [Tests]
# >>> print(u_terme(0), u_0)
# 0 0
# >>> print(u_terme(1), u_1)
# 1 1
# >>> print(u_terme(2), u_2)
# 4 4


# [Q3] Ecrivez une fonction atteint
def atteint(M):
    """Renvoie le rang à partir duquel les termes de la suite u sont supérieurs
    à M.

    CU : M numérique

    Exemple:
    >>> atteint(50623334)
    17
    >>> atteint(299792458)
    19
    """
    assert(type(M) is int or type(M) is float), "M doit être un numérique"

    # On pourrait utiliser la fonction u_terme(), mais celle-ci calcule tous
    # les termes de u_0 à u_n à chaque appel. Si l'on parcourt la suite
    # directement depuis cette fonction, on évite les calculs superflus
    u = 0
    n = 0
    while u <= M:
        n += 1
        u = 3*u+1
    return n

def tester():
    doctest.testmod(verbose=True)
