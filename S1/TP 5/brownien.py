# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°5 donné le 10/10/2014 - Mouvement brownien
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_itcond_tortue.html

from turtle import *
from random import randint

# Constantes
COTE = 400

# Fonctions
def tortue_sortie(cote):
    """
    Indique si la tortue est en dehors du carré de coté 'cote' centré en
    l'origine.

    CU : cote entier strictement positif 

    Exemple :
    >>> tortue_sortie(400)
    """
    assert(type(cote) is int and cote > 0), "cote doit être un entier \
strictement positif"

    return not (xcor() >= -cote/2 and xcor() <= cote/2 and \
        ycor() >= -cote/2 and ycor() <= cote/2)

def carre(cote):
    """
    Dessine un carré bleu de coté 'cote' centré en l'origine.

    CU : cote entier strictement positif

    Exemple :
    >>> carre(50)
    """
    assert(type(cote) is int and cote > 0), "cote doit être un entier \
strictement positif"

    pencolor("blue")
    penup()
    goto(-cote/2, -cote/2)
    pendown()
    for i in range(0, 4):
        forward(cote)
        left(90)

def mouvement_brownien():
    """
    Applique à la tortue une étape du mouvement brownien.

    Exemple :
    >>> mouvement_brownien()
    """
    left(randint(0, 359))
    forward(randint(10, 30))


# Mise en place
carre(COTE)
penup()
pencolor("green")
goto(0, 0)
pendown()

# Mouvement
while not tortue_sortie(COTE):
    mouvement_brownien()
