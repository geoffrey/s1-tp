# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°5 donné le 10/10/2014 - Billard
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_itcond_tortue.html

from turtle import *

def dessiner_billard(dx, dy):
    """
    Dessine un billard centré en l'origine de coté 'dx' × 'dy'.

    CU : dx et dy entiers strictement positifs

    Exemple :
    >>> dessiner_billard(400, 300)
    """
    assert(type(dx) is int and dx > 0), "dx doit être un entier strictement \
positif"
    assert(type(dy) is int and dy > 0), "dy doit être un entier strictement \
positif"
    penup()
    pencolor("red")
    goto(-dx//2, -dy//2)
    pendown()
    begin_fill()
    for i in range(0, 4):
        if i%2:
            forward(dy)
        else:
            forward(dx)
        left(90)
    color("green")
    end_fill()

PAS = 1

def billard(dx, dy, x, y, angle, bandes):
    """
    Dessine un billard centré en l'origine de coté 'dx' × 'dy' et la
    trajectoire de la bille de position initiale ('x', 'y') et d'angle initial
    'angle' jusqu'à 'bandes' bandes.

    CU : dx, dy et bandes entiers strictement positifs, x, y et angle entiers
    
    Exemple :
    >>> billard(400, 300, 50, -25, 80, 5)
    """
    assert(type(dx) is int and dx > 0), "dx doit être un entier strictement \
positif"
    assert(type(dy) is int and dy > 0), "dy doit être un entier strictement \
positif"
    assert(type(angle) is int and angle > 0), "angle doit être un entier \
strictement positif"
    assert(type(x) is int), "x doit être un entier"
    assert(type(y) is int), "y doit être un entier"
    assert(type(bandes) is int), "bandes doit être un entier"
    
    dessiner_billard(dx, dy)
    penup()
    pencolor("black")
    goto(x, y)
    pendown()
    left(angle)
    while bandes >= 1:
        forward(PAS)
        if xcor() >= dx//2:
            left(180-2*heading())
            bandes += -1
        if xcor() <= -dx//2:
            left(180-2*heading())
            bandes += -1
        if ycor() >= dy//2:
            left(-2*heading())
            bandes += -1
        if ycor() <= -dy//2:
            left(-2*heading())
            bandes += -1

# Exemple de test :
# 
