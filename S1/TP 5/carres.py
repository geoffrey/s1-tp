# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°5 donné le 10/10/2014 - Dessiner avec des carrés
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_itcond_tortue.html

from turtle import *

# Instructions pour simplifier le code
def suivant():
    """
    Suspend l'éxecution du script, attend la pression de l'utilisateur sur la
    touche <Entrée> puis efface l'écran de turtle.
    """
    input("Appuyez sur <Entrée> pour continuer")
    clearscreen()


# [Question 1] Réalisez une procédure nommée carre

def carre(c):
    """
    Dessine un carré de coté 'c' depuis l'état courant de la tortue.

    CU : c entier strictement positif
    """
    assert(type(c) is int and c > 0), "c doit être un entier strictement positif"

    pendown()
    for i in range(0, 4):
        forward(c)
        left(90)

print("Test de carre()")
carre(50)

# La tortue est revenue à son point de départ

suivant()


# [Question 2] Dix carrés alignés

ESPACEMENT = 5
COTE = 50
NOMBRE = 10

origine = -(ESPACEMENT + COTE)*NOMBRE//2

print("Dix carrés alignés")

penup()
goto(origine, 0)
for i in range(0, NOMBRE):
    carre(COTE)
    penup()
    forward(COTE + ESPACEMENT)
    # goto(xcor() + COTE + ESPACEMENT, ycor()) # Alternative

suivant()


# [Question 3] Un carré de cent carrés

print("Un carré de cent carrés")

penup()
goto(origine, origine)
for y in range(0, NOMBRE):
    for x in range(0, NOMBRE):
        carre(COTE)
        penup()
        forward(COTE + ESPACEMENT)
        # goto(xcor() + COTE + ESPACEMENT, ycor()) # Alternative
    goto(origine, origine + (COTE + ESPACEMENT) * (y + 1))
    # Alternative
        # forward(-(COTE + ESPACEMENT)*NOMBRE) # Retour à gauche
        # left(90)
        # forward(COTE + ESPACEMENT) # Montée
        # right(90)

suivant()


# [Question 4] Cinquante carrés emboîtés

print("Cinquante carrés emboîtés")

NOMBRE_4 = 50
INTERVALLE = 10

origine_4 = -(NOMBRE_4*INTERVALLE)//2

penup()
goto(origine_4, origine_4)

for j in range(0, NOMBRE_4):
    carre( (j + 1) * INTERVALLE)

suivant()


# [Question 5] Réalisez une procédure carre_tournant

def carre_tournant(n):
    """
    Dessine 'n' carrés de coté 100 pivotant autour de la position courante de
    la tortue.

    CU : n entier strictement positif
    """
    assert(type(n) is int and n > 0), "n doit être un entier strictement positif"


    for i in range(0, n):
        carre(100)
        left(360/n)

print("Sept carrés pivotant")

goto(0, 0)
carre_tournant(7)
