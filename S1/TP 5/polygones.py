# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°5 donné le 10/10/2014 - Dessiner des polygones réguliers
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_itcond_tortue.html

from turtle import *

# Instructions pour simplifier le code
def suivant():
    """
    Suspend l'éxecution du script, attend la pression de l'utilisateur sur la
    touche <Entrée> puis efface l'écran de turtle.
    """
    input("Appuyez sur <Entrée> pour continuer")
    clearscreen()

DECALAGEX = 0
DECALAGEY = -50
ECART = 150
COTE = 75
def deplacer(x, y):
    """
    Déplace la tortue sans tracer de trait à une position ('x', 'y') sur
    un tableau virtuel.

    CU : x et y entiers

    Exemple :
    >>> deplacer(-1, 1)
    """
    assert(type(x) is int), "x doit être un entier"
    assert(type(y) is int), "y doit être un entier"

    penup()
    goto(x*ECART + DECALAGEX, y*ECART + DECALAGEY)


# Cas des polygones convexes

# Q1

# Cet angle est de 360°/n
# En effet au total la tortue aura effecuté un tour complet (360°), mais en
# n fois. Il faut donc diviser les 360° par n.


# Q2 Réalisez une procédure nommée polygone_reg_convexe

def polygone_reg_convexe(n, l):
    """
    Dessine un polygone régulier convexe à 'n' côtés de longueur 'l' depuis
    l'état scourant de la tortue

    CU : n et l entiers strictement positifs

    Exemple :
    >>> polygone_reg_convexe(5, 100)
    """
    assert(type(n) is int and n > 0), "n doit être un entier strictement \
positif"
    assert(type(l) is int and l > 0), "l doit être un entier strictement \
positif"

    pendown()
    for i in range(0, n):
        left(360/n)
        forward(l)


# Q3 Réalisez la figure Quatre polygones convexes.

print("Quatre polygones convexes")
deplacer(-1, 1)
polygone_reg_convexe(4, COTE)
deplacer(1, 1)
polygone_reg_convexe(5, COTE)
deplacer(-1, -1)
polygone_reg_convexe(6, COTE)
deplacer(1, -1)
polygone_reg_convexe(7, COTE)


suivant()


# Cas des polygones étoilés


# Q1 Réalisez une procédure nommée polygone_etoile

def polygone_etoile(n, l, k):
    """
    Dessine un polygone régulier étoilé à 'n' côtés de longueur 'l' avec 'k'
    étant l'ordre de parcours des sommets depuis l'état courant de la tortue.

    CU : n, l et k entiers strictement positifs

    Exemple :
    >>> polygone_etoile(7, 100, 3)
    """
    assert(type(n) is int and n > 0), "n doit être un entier strictement \
positif"
    assert(type(l) is int and l > 0), "l doit être un entier strictement \
positif"
    assert(type(k) is int and k > 0), "k doit être un entier strictement \
positif"

    pendown()
    for i in range(0, n):
        left(k*360/n)
        forward(l)


# Q2 Réalisez la figure Quatre polygones étoilés.

print("Quatre polygones étoilés")
deplacer(-1, 1)
polygone_etoile(5, COTE, 2)
deplacer(1, 1)
polygone_etoile(7, COTE, 3)
deplacer(-1, -1)
polygone_etoile(8, COTE, 3)
deplacer(1, -1)
polygone_etoile(9, COTE, 5)


# Q3 Peut-on dessiner un hexagone étoilé ?

# L'hexagone en bas à gauche de la figure Quatre polygones étoilés est un
# hexagone étoilé, donc oui.
