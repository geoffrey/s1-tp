# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°5 donné le 10/10/2014 - Le module turtle
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_itcond_tortue.html

from turtle import *


# [Question 2]

# Elle fait avancer la tortue dans la direction qui lui a été donné par les
# fonctions left() et right().


# [Question 3]

# Elle dessine un trait uniquement si la fonction pendown() a été appelée plus
# récemnent que la fonction penup().


# [Question 4]

# Elle recule, c'est à dire qu'elle va dans le sens inverse de celui où elle
# irait si on avait donné un argument positif.


# [Question 5]

# Il doit s'exprimer en degrés. On peut le vérifier en testant quelques valeurs
# connues : 360 (tour complet), 180 (demi-tour), 90 (quart de tour)...


# [Question 6]

# Elle déplace la tortue aux points de coordonnées (-200, 90), soit en haut
# à gauche. Elle dessine un trait pour les mêmes conditions que la fonction
# forward, c'est à dire si la fonction pendown() a été appelée plus
# récemnent que la fonction penup().


# [Question 7]

# Définition de constantes pour rendre le code plus compréhensible
TRAIT1X = -200
TRAIT1Y = 170
TRAIT2X = -100
TRAIT2Y = 90
LONGUEUR = 150
ANGLE = 30

# Trait 1
penup()
goto(TRAIT1X, TRAIT1Y)
left(ANGLE)
pencolor('red')
pendown()
forward(LONGUEUR)

# Trait 2
penup()
goto(TRAIT2X, TRAIT2Y)
# On ne change pas l'angle, c'est le même que pour le trait 1
pencolor('green')
pendown()
forward(LONGUEUR)