# VAN DEN BERGE Manon          - PeiP 12 - 2014/2015
# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°7 donné le 7/11/2014 - Exercices sur les chaînes de caractère
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/seq4_chaines_caracteres.html#exercices

import doctest


# Exercice 30 lignes vides

# Comment en utilisant print une seule fois peut-on imprimer 30 lignes
# vides ? (boucles pour et tant que interdites)

# En utilisant l'opérateur de répétition

# print("\n"*30, end='')


# Exercice Impression verticale

# Réalisez une fonction imprimer_vertical qui imprime un à un les
# caractères de la chaîne qu’on lui donne en paramètre les uns au dessus
# des autres.

def imprimer_vertical(chaine):
    """
    Imprime un à un les caractères de la chaîne donnée en paramètre les uns au
    dessus des autres.
    str → ∅
    CU : chaine du type str
    """
    assert(type(chaine) is str), "chaine doit être du type str"

    for i in chaine:
        print(i)

# >>> imprimer_vertical('Timoleon')
# T
# i
# m
# o
# l
# e
# o
# n


# Exercice Impression à l’envers

# Réalisez d’au moins deux façons différentes une fonction imprimer_envers
# qui imprime la chaîne qu’on lui donne en paramètre en inversant l’ordre
# des caractères.

# Avertissement Utilisez le paramètre optionnel end de la fonction print pour
# éviter les passages à la ligne non désirés.

def imprimer_envers1(chaine):
    """
    Imprime la chaîne donnée en paramètre en inversant l’ordre des caractères.
    str → ∅
    CU : chaine du type str
    """
    assert(type(chaine) is str), "chaine doit être du type str"

    affichage = ''
    for i in chaine:
        affichage = i + affichage
    print(affichage)

# >>> imprimer_envers1('Timoleon')
# noelomiT
# >>> imprimer_envers1('')
#

def imprimer_envers2(chaine):
    """
    Imprime la chaîne donnée en paramètre en inversant l’ordre des caractères.
    str → ∅
    CU : chaine du type str
    """
    assert(type(chaine) is str), "chaine doit être du type str"

    affichage = ''
    if len(chaine) > 0:
        for i in range(-1, -len(chaine)-1, -1):
            print(i, chaine[i])
            affichage += chaine[i]
    print(affichage)

# >>> imprimer_envers2('Timoleon')
# noelomiT
# >>> imprimer_envers2('')
#


# Exercice Miroir

# Réalisez une fonction miroir qui renvoie une chaîne contenant les
# caractères de la chaîne qu’on lui donne en paramètre dans l’ordre
# inverse.

def miroir(chaine):
    """
    Renvoie une chaîne contenant les caractères de la chaîne donnés en paramètre
    dans l’ordre inverse.
    str → str
    CU : chaine du type str

    >>> miroir ('Timoleon')
    'noelomiT'
    >>> miroir ('')
    ''
    """
    assert(type(chaine) is str), "chaine doit être du type str"

    retour = ''
    if len(chaine) > 0:
        for i in range(-1, -len(chaine)-1, -1):
            retour += chaine[i]
    return retour

# Exercice Palindrome

# Un palindrome est un mot qui se lit de la même façon de gauche à droite
# et de droite à gauche. Par exemple, ICI, ELLE et RADAR sont des
# palindromes.

# Par extension, une chaîne de caractères est dite palindromique si la
# liste de ses caractères parcourus de gauche à droite est la même que
# celle de ses caractères parcourus de droite à gauche. Par exemple, la
# chaîne 'A, 1 b+b 1 ,A' est palindromique

# Le but est de réaliser un prédicat est_palindromique qui renvoie

# True si la chaîne passée en paramètre est palindromique
# False dans le cas contraire.
# >>> est_palindromique ('Timoleon')
# False
# >>> est_palindromique ('RADAR')
# True
# >>> est_palindromique ('A, 1 b+b 1 ,A')
# True

# Réalisez ce prédicat en utilisant la fonction miroir de l’exercice précédent.

def est_palindromique1(chaine):
    """
    Indique si la chaîne passée en paramètre est palindromique
    str → bool
    CU : mot est du type str

    >>> est_palindromique1('Timoleon')
    False
    >>> est_palindromique1('A, 1 b+b 1 ,A')
    True
    >>> est_palindromique1('')
    True
    """
    assert(type(chaine) is str), "chaine doit être du type str"

    return chaine == miroir(chaine)

# Réalisez une autre version sans utiliser la fonction miroir.

def est_palindromique2(chaine):
    """
    Indique si la chaîne passée en paramètre est palindromique
    str → bool
    CU : mot est du type str

    >>> est_palindromique1('Timoleon')
    False
    >>> est_palindromique1('A, 1 b+b 1 ,A')
    True
    >>> est_palindromique1('')
    True
    """
    assert(type(chaine) is str), "chaine doit être du type str"

    longueur = len(chaine) # On crée une variable pour éviter
                           # de trop appeler len()
    if longueur > 0:
        for i in range(longueur):
            if chaine[longueur-i-1] != chaine[i]:
                return False
    return True


# Exercice Longueur d’une chaîne

# La fonction len renvoie la longueur de la chaîne de caractères qu’on lui
# passe en paramètre.

# Sans utiliser cette fonction, réalisez une fonction qui fait exactement
# la même chose.

def my_len(chaine):
    """
    Renvoie la longueur de la chaîne de caractères passée en paramètre.
    str → int
    CU : chaine est du type str

    >>> my_len ('')
    0
    >>> my_len ('abcd')
    4
    >>> my_len ('esope reste ici et se repose')
    28
    """
    assert(type(chaine) is str), "chaine doit être du type str"

    a = 0
    for i in chaine[:]:
        a += 1
    return a


# Exercice Suppression d’un caractère

# Réalisez une fonction qui renvoie une chaîne obtenue en supprimant le
# caractère d’indice i d’une chaîne s, l’indice et la chaîne s étant
# passés en paramètres.

def sauf(s, i):
    """
    Renvoie une chaîne obtenue en supprimant le caractère d’indice
    i d’une chaîne s, l’indice et la chaîne s étant passés en paramètres.
    (str, int) → str
    CU : s du type str et 0 ≤ i < len(s)

    >>> sauf('Timoleon', 3)
    'Timleon'
    """
    assert(type(s) is str), "s doit être du type str"
    assert(i >= 0 and i < len(s)), "i doit être dans l'intervalle [0, len(s)["

    return s[:i] + s[i + 1:]

# Vous pourrez supposer que l’indice est un entier positif ou nul.

# >>> s = 'Timoleon'
# >>> for i in range(len(s)):
# ...     print(sauf(s, i))
# ...
# imoleon
# Tmoleon
# Tioleon
# Timleon
# Timoeon
# Timolon
# Timolen
# Timoleo


# Que renvoie votre fonction si l’indice passé en paramètre est négatif ?

# En premier test, la fonction déclenche une erreur d'assertion.
# En enlevant les assertions, l'instruction sauf(s, -i) (pour i positif)


# Exercice Nombre d’occurrences

# Réalisez une fonction qui renvoie le nombre d’occurrences d’un caractère
# dans une chaîne, le caractère et la chaîne étant passés en paramètre.

def nbre_occurrences(caractere, chaine):
    """
    Renvoie le nombre d’occurrences d’un caractère dans une chaîne, le caractère
    et la chaîne étant passés en paramètre.
    (str, str) → int
    CU : caractere de type str de taille 1 et chaine de type str

    >>> nbre_occurrences ('o', 'Timoleon')
    2
    >>> nbre_occurrences ('y', 'Timoleon')
    0
    >>> nbre_occurrences (' ', 'esope reste ici et se repose')
    5
    """
    assert(type(caractere) is str and len(caractere) == 1), \
        "caractere doit être du type str et de taille 1"
    assert(type(chaine) is str), "chaine doit être du type str"

    nombre = 0
    for i in chaine:
        if i == caractere:
            nombre += 1
    return nombre


# Exercice Suppression des occurences d’un caractère

# Réalisez une fonction qui renvoie une chaîne obtenue en supprimant
# toutes les occurrences d’un caractère dans une chaîne, ce caractère et
# cette chaîne étant passés en paramètres.

def supprime_car(caractere, chaine):
    """
    Renvoie une chaîne obtenue en supprimant toutes les occurrences d’un
    caractère dans une chaîne, ce caractère et cette chaîne étant passés en
    paramètres.
    (str, str) → str
    CU : caractere de type str de taille 1 et chaine de type str

    >>> supprime_car ('o', 'Timoleon')
    'Timlen'
    >>> supprime_car ('y', 'Timoleon')
    'Timoleon'
    """
    assert(type(caractere) is str and len(caractere) == 1), \
        "caractere doit être du type str et de taille 1"
    assert(type(chaine) is str), "chaine doit être du type str"

    retour = ''
    for i in chaine:
        if i != caractere:
            retour += i
    return retour


# La phrase esope reste ici et se repose n’est pas un palindrome au sens
# donné dans l’exercice ci-dessus. Mais si on ne regarde que les lettres,
# c’est un palindrome.

# Comment vérifier ce point avec les fonctions supprime_car et
# est_palindromique ?

# On peut utiliser la composée des deux fonctions de la façon suivante
# (chaine étant la chaine dont on souhaite vérifier si elle est palindromique)

# est_palindromique(supprime_car(' ', chaine))

# On peut même enlever les points, les virgules et toute autre ponctuation
# génante

# est_palindromique(supprime_car(' ', supprime_car(',', supprime_car('.', \
#   supprime_car('!', supprime_car('?', chaine))))))

# Cependant, créer une nouvelle fonction qui supprime tous les caractères d'une
# contenus dans une liste serait plus approprié dans ce cas.


# Exercice Remplacement d’un caractère

# Réalisez une fonction qui remplace le caractère d’un indice donné d’une
# chaîne de caractères par une autre chaîne de caractères.

def remplace_indice(chaine, indice, remplacement):
    """
    Remplace le caractère d’un indice donné d’une chaîne de caractères par une
    autre chaîne de caractères.
    (str, int, str) → str
    CU : chaine de type str et indice de type int et remplacement de type str

    >>> remplace_indice ('Timileon',3,'o')
    'Timoleon'
    >>> remplace_indice ('Tixleon',2,'mo')
    'Timoleon'
    """
    assert(type(chaine) is str), "chaine doit être du type str"
    assert(type(indice) is int), "indice doit être du type int"
    assert(type(remplacement) is str), "remplacement doit être du type str"

    return chaine[:indice] + remplacement + chaine[indice + 1:]

# Réalisez une fonction qui remplace dans une chaîne donnée toutes les
# occurences d’un caracère donné par une autre chaîne de caractères.

def remplace_occurrences(chaine, caractere, remplacement):
    """
    Remplace dans une chaîne donnée toutes les occurences d’un caracère donné
    par une autre chaîne de caractères.
    (str, str, str) → str
    CU : chaine de type str et caractere du type str et de taille 1 et
    remplacement de type str

    >>> remplace_occurrences ('@ 3 est le neveu de @ 1er.','@','Napoléon')
    'Napoléon 3 est le neveu de Napoléon 1er.'
    """
    assert(type(chaine) is str), "chaine doit être du type str"
    assert(type(caractere) is str and len(caractere) == 1), \
        "caractere doit être du type str et de taille 1"
    assert(type(remplacement) is str), "remplacement doit être du type str"

    retour = ''
    for i in chaine:
        if i == caractere:
            retour += remplacement
        else:
            retour += i
    return retour



def tester():
    doctest.testmod(verbose=True)
