#Jeu d'Échecs

Jeu d'Échec programmé dans le cadre d'un TP alternatif pour l'enseignement Informatique S1.

##Dépendances

* python ≥ 3.0
* tk

##Lancement
Dans le dossier racine du projet :
```bash
python app.py
```

(en supposant que `python` est dans la variable d'environnement `path` (ou `$PATH` sous Windows) et point vers l’exécutable de Python 3)

##Objectifs non-réalisés

###Objectifs de TP

* Implémenter le jeu d'échecs

Le jeu d'échec n'est pas totalement implémenté, voici la liste non-exhaustive des règles qui ne sont pas implémentées

    * Le Roque
    * La promotion du pion
    * Prise en passant
    * Détection du "pat" (le joueur ne pouvant pas jouer est considéré comme perdant, même s'il n'est pas en échec)

* Implémenter le jeu de dames

Le jeu de dames est tout à fait jouable, bien que certaines règles ne soient pas implémentés. En voici la liste non-exhaustive.

    * L'enlèvement des pièces d'une rafle lors de la fin de celle-ci
    * Partie nulle
    * "Souffler n'est pas jouer" implémenté bien qu'abandonné en 1927 à des fins de démonstration

* Redimensionnement de la fenêtre

Il suffirait d'appeler `PlateauTk.redimCan(min(xMax, yMax))` à chaque redimensionnement de la fenêtre. Cependant j'ai un peu de mal à comprendre comment fonctionne Tk.

* Documenter et tester les fonctions

Chaque fonction possède une brève description de son fonctionnement. L'ajout des types des arguments serait inutile, étant la majorité du temps des int. Le contenu des arguments s'expliquent par leur nom, à défaut par la description de la fonction. De plus, la majorité des fonctions dépendant de l'instance de leur classe, aucune doctest n'a été écrite, car cela aurait nécessité trop de code pour de simples fonctions.

###Objectifs personnels

* Qualité du code

Bien que le code fonctionne, soit plutôt flexible, relativement documenté et utilise la notion d'objet, il est loin d'être irréprochable. Certaines fonctions font probablement plus de calcul que nécessaire, la création de certaines variables pourraient être évité. L'ajout de plus de constante au lieu de valeurs arbitraire contribuerait à la lisibilité du code. Certaines instructions ne sont probablement pas sémantiquement correcte. Certaines parties du code devraient avoir une gestion d'erreurs car elles sont susceptibles d'en avoir.

* Test

N'étant pas un joueur d'Échecs ni de Dames experimenté, je n'ai pas pu tester toutes les possibilités du jeu, il est donc possible qu'il y ait des bogues, qu'elles soient graphiques, entraînant un plantage, ou autorise / défende une action normalement possible dans le jeu original.

* Originalité

Le jeu actuel ne possède aucun point fort (au contraire) qui pourrait le démarquer des jeux d'Échecs existant. Très peu d'utilisateurs pourraient l'utiliser dans une optique autre que le développement ou le test.

Les commentaires affublés d'une marque `TODO` signifient que le code pourrait être amélioré à cet endroit, ou une fonctionnalité pourrait y être ajoutée.
