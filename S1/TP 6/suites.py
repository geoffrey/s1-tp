# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°6 donné le 17/10/2014 - Calculs de suites
# http://www2.lifl.fr/~mailliet/Initprog/TP6.pdf

import doctest

# Racine carrée

def rac_car1(A, p):
    """
    Retourne une approximation de √A (flottant) en calculant le terme de rang p
    de la suite de Héron.

    CU : A numérique ≥ 0, p entier ≥ 0

    >>> rac_car1(0, 10)
    0
    >>> rac_car1(5, 1)
    2.25
    >>> rac_car1(5, 10)
    2.23606797749979
    """
    assert(type(A) is int or type(A) is float), "A doit être un numérique"
    assert(type(p) is int and p >= 0), "p doit être un entier ≥ 0"

    if A == 0: # Cas à différencier pour éviter une division par 0
        return 0
    else:
        x = A/2 # Définition de x_0
        for i in range(1, p+1): # Pour chaque terme de 1 à p (on a déjà x_0)
            x = (x+A/x)/2 # On calcule le terme suivant
        return x # Retour de la valeur

def rac_car2(A):
    """
    Retourne une approximation de √A (flottant) en utilisant la suite de
    Héron.

    CU : A numérique ≥ 0

    >>> rac_car2(0)
    0
    >>> rac_car2(5)
    2.23606797749979
    """
    assert(type(A) is int or type(A) is float), "A doit être un numérique"

    if A == 0: # Cas à différencier pour éviter une division par 0
        return 0
    else:
        x = A/2 # Définition de x_0
        xP = x + 1 # Initialisation par une valeur autre que x pour démarrer
                   # la boucle
        while x != xP: # Tant que Python peut calculer les décimales
            xP = x # La valeur actuelle devient précédente
            x = (x+A/x)/2 # On calcule le terme suivant
        return x # Retour de la valeur


# Somme et produit

def factorielle(k):
    """
    Retourne k! (entier)

    CU : k entier ≥ 0

    >>> factorielle(0)
    1
    >>> factorielle(5)
    120
    """
    assert(type(k) is int and k >= 0), "k doit être un entier ≥ 0"

    f=1 # Initialisation du produit
    for i in range(1, k+1): # De 1 à k
        f = f * i # On ajoute un élément du produit
    return f # Retour de la valeur

def somme(n):
    """
    Retourne la somme des entiers de 1 à n (entier)

    CU : n entier ≥ 0

    >>> somme(0)
    0
    >>> somme(1)
    1
    >>> somme(3)
    6
    """
    assert(type(n) is int and n >= 0), "n doit être un entier ≥ 0"

    s = 0 # Initialisation de la somme
    for i in range(1, n+1): # De 1 à n
        s += i # On ajoute un élement de la somme
    return s # Retour de la valeur

def sommeA(n): # Alternative plus rapide à calculer
    """
    Retourne la somme des entiers de 1 à n (entier)

    CU : n entier ≥ 0

    >>> sommeA(0)
    0
    >>> sommeA(1)
    1
    >>> sommeA(3)
    6
    """
    assert(type(n) is int and n >= 0), "n doit être un entier ≥ 0"

    return n*(n+1)//2


# Exponentielle

def exponentielle(x):
    """
    Retourne une approximation de exp(x) (flottant)

    CU : x numérique

    >>> exponentielle(-5)
    0.006737946999086907
    >>> exponentielle(0)
    1.0
    >>> exponentielle(1)
    2.7182818284590455
    >>> exponentielle(5)
    148.41315910257657
    """
    assert(type(x) is int or type(x) is float), "x doit être un numérique"

    e = 0 # Initialisation de la somme
    eP = 1 # Initialisation par une valeur autre que s pour démarrer la boucle
    i = 0 # Initialisation de l'incrémenteur
    while e != eP: # Tant que Python peut calculer les décimales
        eP = e # La valeur actuelle devient précédente
        e = e + (x ** i) / (factorielle(i)) # On ajoute un élement de la somme
        i += 1 # On incrémente i
    return e # Retour de la valeur


# Sinus

def sinus(X):
    """
    Retourne une approximation de sin(x) (flottant)

    CU : X numérique

    >>> sinus(-5)
    0.9589242746631357
    >>> sinus(0)
    0.0
    >>> sinus(5)
    -0.9589242746631357
    """
    assert(type(X) is int or type(X) is float), "X doit être un numérique"

    s = 0 # Initialisation de la somme
    sP = 1 # Initialisation par une valeur autre que s pour démarrer la boucle
    i = 0 # Initialisation de l'incrémenteur
    while s != sP: # Tant que Python peut calculer les décimales
        sP = s # La valeur actuelle devient précédente
        if i % 2: # Si i est impair alors (-1)^i=-1, si pair =1 
            p = -1
        else:
            p = 1
        s += p * (X ** (2 * i + 1)) / factorielle(2 * i + 1) # On ajoute une
                                                              # partie de la
                                                              # somme
        i += 1 # On incrémente i
    return s # Retour de la valeur


# Logarithme

def ln(X):
    """
    Retourne une approximation de Ln(1+X) (flottant)

    CU : X numérique ∈ [0,1]

    >>> ln(0)
    0.0
    >>> ln(0.5)
    0.4054651081081643
    """
    assert((type(X) is int or type(X) is float) and X >= 0 and X <= 1), \
        "X doit être un numérique ∈ [0,1]"

    l = 0 # Initialisation de la somme
    lP = 1 # Initialisation par une valeur autre que l pour démarrer la boucle
    i = 1 # Initialisation de l'incrémenteur
    while l != lP : # Tant que Python peut calculer les décimales
        lP = l # La valeur actuelle devient précédente
        if i % 2: # Vérifier si i-1 est pair revient à vérifier si i est impair
                  # cela revient à inverser les actions
            p = 1
        else:
            p = -1
        l += p * X**i / i # On ajoute un élement de la somme
        i += 1 # On incrémente i
    return l # Retour de la valeur


def tester():
    doctest.testmod(verbose=True)
