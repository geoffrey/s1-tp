# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# Contrôle de TP du 12/12/2014
# http://www.lifl.fr/~mailliet/Initprog/ctrltpp/

import doctest


##On rappelle que ces codes peuvent être directement obtenues dans Python en utilisant la fonction ord qui renvoie le code (un entier) correspondant au caractère qui lui est passé en paramètre et la fonction réciproque est chr.
##Question 1 Réalisez une fonction chiffrement_lettre_maj, prenant deux paramètres : un caractère carac et un entier n et qui renvoie le résultat du décalage de carac de n lettres vers la droite (décalage cyclique, rappelez-vous, les dernières lettres de l'alphabet sont codées par les premières) .

def chiffrement_lettre_maj(carac, n):
    """
    str, int → str
    Renvoie le résultat du décalage de carac de n lettres vers la droite.

    >>> chiffrement_lettre_maj('A', 3)
    'D'
    >>> chiffrement_lettre_maj('Y', 2)
    'A'
    >>> chiffrement_lettre_maj('W', 4)
    'A'
    """
    ordre = ord(carac)+n
    if ordre < 65:
        ordre += 26
    if ordre > 90:
        ordre += -26
    return chr(ordre)

##Question 2 Réalisez une fonction nommée dechiffrement_lettre_maj qui soit la fonction inverse de chiffrement_lettre_maj.

def dechiffrement_lettre_maj(carac, n):
    """
    str, int → str
    Renvoie le résultat du décalage de carac de n lettres vers la gauche.

    >>> dechiffrement_lettre_maj('D', 3)
    'A'
    >>> dechiffrement_lettre_maj('A', 2)
    'Y'
    >>> dechiffrement_lettre_maj('A', 4)
    'W'
    """
    
    return chiffrement_lettre_maj(carac, -n)


##Question 3 Réalisez un prédicat nommé est_majuscule qui détermine si le caractère passé en paramètre est une lettre majuscule ou non. La fonction (ce prédicat) renverra un booléen.

def est_majuscule(carac):
    """
    str → bool
    Indique si carac est une majuscule

    >>> est_majuscule('A')
    True
    >>> est_majuscule('z')
    False
    """
    ordre = ord(carac)
    return ordre >= 65 and ordre <= 90

##Question 4 Réalisez une fonction chiffrement qui prend une chaîne de caractères et un entier d en paramètres, et qui renvoie une chaîne de caractères chiffrée avec le décalage d.

def chiffrement(chaine, d):
    """
    str, int → str
    Renvoie une chaîne de caractères chiffrée avec le décalage d.
    >>> chiffrement('YOYO', 3)
    'BRBR'
    >>> chiffrement("CESAR, IL EST TROP FORT !", 2)
    'EGUCT, KN GUV VTQR HQTV !'
    """
    chiffrat = ''
    for i in chaine:
        if est_majuscule(i):
            chiffrat += chiffrement_lettre_maj(i, d)
        else:
            chiffrat += i
    return chiffrat

##Question 5 Réalisez également la fonction inverse qui permet de déchiffrer un message.

def dechiffrement(chaine, d):
    """
    str, int → str
    Renvoie une chaîne de caractères déchiffrée avec le décalage d.
    >>> dechiffrement('BRBR', 3)
    'YOYO'
    >>> dechiffrement("EGUCT, KN GUV VTQR HQTV !", 2)
    'CESAR, IL EST TROP FORT !'
    """
    return chiffrement(chaine, -d)

##Question 6 Quel est le message que César a codé par SWGN DGCW OGVKGT RTQHGUUGWT ?

##>>> dechiffrement('SWGN DGCW OGVKGT RTQHGUUGWT ?', 2)
##'QUEL BEAU METIER PROFESSEUR ?'

##Question 7 Il est maintenant temps de s’intéresser aux lettres minuscules. Vous allez réaliser les fonctions nécessaires au traitement des lettres minuscules.

def chiffrement_lettre_min(carac, n):
    """
    str, int → str
    Renvoie le résultat du décalage de carac de n lettres vers la droite.

    >>> chiffrement_lettre_min('a', 3)
    'd'
    >>> chiffrement_lettre_min('y', 2)
    'a'
    >>> chiffrement_lettre_min('w', 4)
    'a'
    """
    ordre = ord(carac)+n
    if ordre < 97:
        ordre += 26
    if ordre > 122:
        ordre += -26
    return chr(ordre)

def dechiffrement_lettre_min(carac, n):
    """
    str, int → str
    Renvoie le résultat du décalage de carac de n lettres vers la gauche.

    >>> dechiffrement_lettre_min('d', 3)
    'a'
    >>> dechiffrement_lettre_min('a', 2)
    'y'
    >>> dechiffrement_lettre_min('a', 4)
    'w'
    """
    
    return chiffrement_lettre_min(carac, -n)

def est_minuscule(carac):
    """
    str → bool
    Indique si carac est une majuscule

    >>> est_minuscule('a')
    True
    >>> est_minuscule('Z')
    False
    """
    ordre = ord(carac)
    return ordre >= 97 and ordre <= 122

def chiffrement2(chaine, d):
    """
    str, int → str
    Renvoie une chaîne de caractères chiffrée avec le décalage d.
    >>> chiffrement2('Yoyo', 3)
    'Brbr'
    >>> chiffrement2 ("Jules Cesar dit : Tu quoque mi fili", 2)
    'Lwngu Eguct fkv : Vw swqswg ok hknk'
    """
    chiffrat = ''
    for i in chaine:
        if est_majuscule(i):
            chiffrat += chiffrement_lettre_maj(i, d)
        elif est_minuscule(i):
            chiffrat += chiffrement_lettre_min(i, d)
        else:
            chiffrat += i
    return chiffrat

def dechiffrement2(chaine, d):
    """
    str, int → str
    Renvoie une chaîne de caractères déchiffrée avec le décalage d.
    >>> dechiffrement2('Brbr', 3)
    'Yoyo'
    >>> dechiffrement2("Lwngu Eguct fkv : Vw swqswg ok hknk", 2)
    'Jules Cesar dit : Tu quoque mi fili'
    """
    return chiffrement2(chaine, -d)

def tester():
    doctest.testmod(verbose=True)
