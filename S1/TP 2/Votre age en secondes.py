# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°2 donné le 19/09/2014 - Votre âge en secondes
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_donnees_expressions.html

# Q1
ref_an = 1900
ref_mois = 1
ref_jour = 1

# Q2
nbre_sec_jour = 24 * 60 * 60

# Q3
nbre_sec_an = 365.2425 * nbre_sec_jour

# Q4
nbre_sec_mois = nbre_sec_an / 12

# Q5
aujourdhui_jour = 19
aujourdhui_mois = 9
aujourdhui_an = 2014

# Q6
nbre_an_entre_1900_et_aujourdhui = aujourdhui_an - ref_an
nbre_mois_entre_1900_et_aujourdhui = aujourdhui_mois - ref_mois
nbre_jour_entre_1900_et_aujourdhui = aujourdhui_jour - ref_jour
nbre_sec_entre_1900_et_aujourdhui = \
    nbre_an_entre_1900_et_aujourdhui * nbre_sec_an \
    + nbre_mois_entre_1900_et_aujourdhui * nbre_sec_mois \
    + nbre_jour_entre_1900_et_aujourdhui * nbre_sec_jour

print("Q6", "Secondes entre la date de référence et aujourd’hui :", \
    nbre_sec_entre_1900_et_aujourdhui)

# Q7
naissance_jour = 14
naissance_mois = 2
naissance_an = 1997

nbre_an_entre_1900_et_naissance = naissance_an - ref_an
nbre_mois_entre_1900_et_naissance = naissance_mois - ref_mois
nbre_jour_entre_1900_et_naissance = naissance_jour - ref_jour
nbre_sec_entre_1900_et_naissance = \
    nbre_an_entre_1900_et_naissance * nbre_sec_an \
    + nbre_mois_entre_1900_et_naissance * nbre_sec_mois \
    + nbre_jour_entre_1900_et_naissance * nbre_sec_jour


# Q8
mon_age_en_secondes = \
    nbre_sec_entre_1900_et_aujourdhui - nbre_sec_entre_1900_et_naissance
print("Q8", "Mon âge en secondes :", mon_age_en_secondes)

# Q9
def nbre_sec_depuis_1900(jour, mois, an):
    """
    Calcule le nombre approximatif de secondes écoulées entre la date de
    référence (le 1er janvier 1900) à 0h00 et la date passée en paramètre

    CU : jour entier ; 1 ≤ jour ≤ 31 ; mois entier ; 1 ≤ mois ≤ 12 ;
    an entier ; an ≥ 1900 ; la date doit avant aujourd'hui

    Exemple :

    >>> nbre_sec_depuis_1900(14, 2, 1997)
    3064777290.0
    """
    nbre_jour_entre_1900_et_date = jour - ref_jour
    nbre_mois_entre_1900_et_date = mois - ref_mois
    nbre_an_entre_1900_et_date = an - ref_an
    nbre_sec_entre_1900_et_date = \
        nbre_an_entre_1900_et_date * nbre_sec_an \
        + nbre_mois_entre_1900_et_date * nbre_sec_mois \
        + nbre_jour_entre_1900_et_date * nbre_sec_jour
    return nbre_sec_entre_1900_et_date

# Q10
mon_age_en_secondes = \
    nbre_sec_depuis_1900(aujourdhui_jour, aujourdhui_mois, aujourdhui_an) \
    - nbre_sec_depuis_1900(naissance_jour, naissance_mois, naissance_an)
print("Q10", "Mon âge en secondes (via nbre_sec_depuis_1900) :", \
    mon_age_en_secondes)

# Q11
from datetime import *

aujourdhui = date.today()

aujourdhui_jour = aujourdhui.day
aujourdhui_mois = aujourdhui.month
aujourdhui_an = aujourdhui.year

mon_age_en_secondes = \
    nbre_sec_depuis_1900(aujourdhui_jour, aujourdhui_mois, aujourdhui_an) \
    - nbre_sec_depuis_1900(naissance_jour, naissance_mois, naissance_an)
print("Q11", "Mon âge en secondes (via datetime) :", \
    mon_age_en_secondes)


# Q12
def age_en_secondes(jour, mois, an):
    """
    Calcule le nombre approximatif de secondes écoulées depuis la date passée
    en paramètre.

    CU : jour entier ; 1 ≤ jour ≤ 31 ; mois entier ; 1 ≤ mois ≤ 12 ;
    an entier ; an ≥ 1900 ; la date doit avant aujourd'hui

    Exemple (si nous sommes le 19/09/2014) :

    >>> age_en_secondes(14, 2, 1997)
    555308406.0
    """
    ajd = date.today()
    return nbre_sec_depuis_1900(ajd.day, ajd.month, ajd.year) \
        - nbre_sec_depuis_1900(jour, mois, an)

mon_age_en_secondes = \
    age_en_secondes(naissance_jour, naissance_mois, naissance_an)
print("Q12", "Mon âge en secondes (via age_en_secondes) :", \
    mon_age_en_secondes)
