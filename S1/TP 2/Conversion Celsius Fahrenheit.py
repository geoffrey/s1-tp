# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°2 donné le 19/09/2014 - Conversion Celsius ↔ Fahrenheit
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_donnees_expressions.html

# Q1
C = 20

# Q2
F = 9/5*C+32

# Q3
print("Une température de", C, "°C correspond à une température de", F, "F.")

# Q4
F2 = 75
C2 = 5/9*(F2-32)
print("Une température de", F2, "F correspond à une température de", C2, "°C.")

# Q5
def celsius_en_fahrenheit(celsius):
    """
    Convertit une température en degrés Celsius en degrés Fahrenheit.

    CU : celsius numérique

    Exemple :

    >>> celsius_en_fahrenheit(20)
    68.0
    """
    return 9/5*celsius+32

help(celsius_en_fahrenheit)

print("Q5", "Une température de 21 °C correspond à une température de", \
    celsius_en_fahrenheit(21), "F.")
print("Q5", "Une température de 34 °C correspond à une température de", \
    celsius_en_fahrenheit(34), "F.")
print("Q5", "Une température de 55 °C correspond à une température de", \
    celsius_en_fahrenheit(55), "F.")

# Q6
def fahrenheit_en_celsius(fahrenheit):
    """
    Convertit une température en degrés Fahrenheit en degrés Celsius.

    CU : fahrenheit numérique

    Exemple :

    >>> fahrenheit_en_celsius(75)
    23.88888888888889
    """
    return 5/9*(fahrenheit-32)

# Q7
# On fait une composition de fonction (au moins deux fois pour vérifier les
# deux sens) avec un nombre choisi arbitrairement

nombreTest = 42

test = celsius_en_fahrenheit( \
    fahrenheit_en_celsius( \
        celsius_en_fahrenheit( \
            fahrenheit_en_celsius(nombreTest)))) == nombreTest
print("Q7", "Le test sur la réciprocité des fonctions a retourné :", test)
