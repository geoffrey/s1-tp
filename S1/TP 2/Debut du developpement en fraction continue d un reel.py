# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°2 donné le 19/09/2014 - Début du developpement en fraction continue d’un
# réel
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_donnees_expressions.html

# Q1
from math import floor

# Q2
def afficher_debut(x):
    """
    Réalise le début du développement en fraction continue d’un nombre et
    affiche les parties entières calculées.

    CU : x numérique

    Exemple :

    >>> afficher_debut(3.14159265359)
    a0 =  3
    a1 =  7
    a2 =  15
    """
    # Calcul des valeurs
    a0 = floor(x)
    y = (x - a0) ** -1
    a1 = floor(y)
    z = (y - a1) ** -1
    a2 = floor(z)

    # Affichage des valeurs
    print("a0 = ", a0)
    print("a1 = ", a1)
    print("a2 = ", a2)

# Q3
from math import pi

# Q4
afficher_debut(pi)

def afficher_debut_bis(x):
    """
    Réalise le début du développement en fraction continue d’un nombre et
    affiche une approximation de ce nombre sous la forme d'une fraction
    continue.

    CU : x numérique

    Exemple :

    >>> afficher_debut_bis(3.14159265359)
    3 + 1 / (7 + 1 / 15)
    """
    # Calcul des valeurs
    a0 = floor(x)
    y = (x - a0) ** -1
    a1 = floor(y)
    z = (y - a1) ** -1
    a2 = floor(z)

    # Affichage des valeurs
    print(str(a0) + " + 1 / (" + str(a1) + " + 1 / " + str(a2) + ")")

afficher_debut_bis(pi)
