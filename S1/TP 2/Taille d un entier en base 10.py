# PREUD'HOMME BONTOUX Geoffrey - PeiP 12 - 2014/2015
# TP n°2 donné le 19/09/2014 - Taille d'un entier en base 10
# http://www.fil.univ-lille1.fr/~wegrzyno/portail/Info/Doc/HTML/tp_donnees_expressions.html

# Q1
from math import log10

# Q2
from math import floor
print("Q2", "floor(3.14159265359) retourne", floor(3.14159265359))

# Q3
def taille(entier):
    """
    Calcule la taille décimale d’un entier positif.

    CU : entier int ≥ 0

    Exemple :

    >>> taille(2014)
    4
    """
    return floor(log10(entier)) + 1

# Q4
print("Q4", "taille(2**100) retourne", taille(2**100))

# Q5
# Le code qui suit a été éxecuté dans l'interpréteur puis inseré ici en tant
# que commentaire pour garder le fichier valide car il provoque des erreurs

# >>> taille(-2014)
# Traceback (most recent call last):
#   File "<stdin>", line 1, in <module>
#   File "<stdin>", line 12, in taille
# ValueError: math domain error

# Lors de l'éxecution de ce script, Python détecte une erreur. Il affiche donc
# dans la console la liste des fonctions appelées en dernier afin de nous aider
# à voir d'où vient l'erreur. Ici, cette dernière vient de la fonction taille(),
# plus précisément de la ligne contenant l'instruction suivante :
# ```return floor(log10(entier)) + 1```
# Le texte de l'erreur est le suivant : "ValueError: math domain error". Cela
# signifie qu'une fonction a été appelée avec un argument en dehors de son
# domaine de définition. La fonction en question est log10 (nous pouvons le
# vérifier en tapant dans l'interpréteur ```log10(-2014)```, qui est une étape
# de la fonction taille(), ce qui affiche la même erreur), qui est définie
# pour tout réel strictement positif. Or, -2014 n'en est pas un, ce qui
# déclenche l'erreur.
